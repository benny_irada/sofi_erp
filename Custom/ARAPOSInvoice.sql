if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sfnAPRptInvoiceSub]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[sfnAPRptInvoiceSub]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sfnARRptInvoiceSub]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[sfnARRptInvoiceSub]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sfnAPRptInvoice]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[sfnAPRptInvoice]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sfnARRptInvoice]') and xtype in (N'FN', N'IF', N'TF'))
drop function [dbo].[sfnARRptInvoice]
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.sfnAPRptInvoiceSub
(
	@intVendorID int, 
	@sinStatusID smallint, 
	@datDate datetime = NULL
)
RETURNS TABLE
AS
RETURN (
	SELECT dbo.tblRecInvoice.BillVendorID AS VendorID, 
		0 AS Type, 
		dbo.tblRecInvoice.RecInvoiceID AS DocID, 
		dbo.tblRecInvoice.rivDocument AS DocNo, 
		'Invoice' AS typDesc, 
		dbo.tblRecInvoice.rivDocDate AS DocDate, 
		dbo.tblRecInvoice.rivDueDate AS DocDueDate,
--By Antoni 16 Mar 2006
--By Jemi 19 May 2006, make lenght to 255 Max
		--ISNULL(dbo.sfnGetRefNoRecInvoice(dbo.tblRecInvoice.RecInvoiceID, 'PO', 11, ', '), '-') AS PONo,
		CONVERT(varchar(255), ISNULL(dbo.sfnGetRefNoRecInvoice(dbo.tblRecInvoice.RecInvoiceID, 'PO', 11, ', '), '-')) AS PONo,
--**************************** 
--**************************** 
		dbo.tblRecInvoice.TermPmtID, 
		dbo.tblRecInvoice.CurrencyID, 
--By Jemi 5 Oct 2005
		dbo.tblRecInvoice.JournalTypeID,
--******************************
		dbo.tblRecInvoice.rivTotNetAmt AS Amount, 
		dbo.tblRecInvoice.COAID, 
		dbo.tblRecInvoice.StatusID 
	FROM dbo.tblRecInvoice 
	WHERE ((dbo.tblRecInvoice.Deleted = 0) AND 
		(dbo.tblRecInvoice.StatusID <> 5) AND 
		(dbo.tblRecInvoice.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblRecInvoice.StatusID ELSE @sinStatusID END) AND
		(dbo.tblRecInvoice.BillVendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblRecInvoice.BillVendorID ELSE @intVendorID END) AND
		(dbo.tblRecInvoice.rivDocDate <= CASE WHEN @datDate IS NULL THEN dbo.tblRecInvoice.rivDocDate ELSE @datDate END))
	UNION ALL
	SELECT dbo.tblAPCN.VendorID, 
		1 AS Type, 
		dbo.tblAPCN.APCNID AS DocID, 
		dbo.tblAPCN.APCNCD AS DocNo, 
		'Credit Note' AS typDesc, 
		dbo.tblAPCN.pcnDate AS DocDate, 
		dbo.tblAPCN.pcnDate AS DocDueDate, 
--By Antoni 16 Mar 2006
		'-' AS PONo,
--**************************** 
		- 1 AS TermPmtID, 
		dbo.tblAPCN.CurrencyID, 
--By Jemi 5 Oct 2005
		dbo.tblAPCN.JournalTypeID,
--******************************
		dbo.tblAPCN.pcnAmount AS Amount, 
		dbo.tblAPCN.COAID,
		dbo.tblAPCN.StatusID
	FROM dbo.tblAPCN 
	WHERE ((dbo.tblAPCN.StatusID <> 5) AND 
		(dbo.tblAPCN.Deleted = 0) AND 
		(dbo.tblAPCN.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblAPCN.StatusID ELSE @sinStatusID END) AND
		(dbo.tblAPCN.VendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblAPCN.VendorID ELSE @intVendorID END) AND
		(dbo.tblAPCN.pcnDate <= CASE WHEN @datDate IS NULL THEN dbo.tblAPCN.pcnDate ELSE @datDate END))
	UNION ALL
	SELECT dbo.tblAP.VendorID, 
		2 AS Type, 
		dbo.tblAP.APID AS DocID, 
		dbo.tblAP.apyDocNo AS DocNo, 
		'A/P' AS typDesc, 
		dbo.tblAP.apyDocDate AS DocDate, 
		dbo.tblAP.apyDueDate AS DocDueDate, 
--By Antoni 16 Mar 2006
		'-' AS PONo,
--**************************** 
		dbo.tblAP.TermPmtID, 
		dbo.tblAP.CurrencyID, 
--By Jemi 5 Oct 2005
		dbo.tblAP.JournalTypeID,
--******************************
		dbo.tblAP.apyAmount AS Amount, 
		dbo.tblAP.COAID,
		dbo.tblAP.StatusID  
	FROM dbo.tblAP 
	WHERE ((dbo.tblAP.Deleted = 0) AND 
		(dbo.tblAP.StatusID <> 5) AND 
		(dbo.tblAP.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblAP.StatusID ELSE @sinStatusID END) AND
		(dbo.tblAP.VendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblAP.VendorID ELSE @intVendorID END) AND
		(dbo.tblAP.apyDocDate <= CASE WHEN @datDate IS NULL THEN dbo.tblAP.apyDocDate ELSE @datDate END))
	UNION ALL
	SELECT dbo.tblAPDN.VendorID, 
		3 AS Type, 
		dbo.tblAPDN.APDNID AS DocID, 
		dbo.tblAPDN.APDNCD AS DocNo, 
		'Debit Note' AS typDesc, 
		dbo.tblAPDN.pdnDate AS DocDate, 
		dbo.tblAPDN.pdnDate AS DocDueDate,
--By Antoni 16 Mar 2006
		'-' AS PONo,
--****************************  
		- 1 AS TermPmtID, 
		dbo.tblAPDN.CurrencyID, 
--By Jemi 5 Oct 2005
		dbo.tblAPDN.JournalTypeID,
--******************************
		dbo.tblAPDN.pdnAmount * (-1) AS Amount, 
		dbo.tblAPDN.COAID,
		dbo.tblAPDN.StatusID
	FROM dbo.tblAPDN 
	WHERE ((dbo.tblAPDN.StatusID <> 5) AND 
		(dbo.tblAPDN.Deleted = 0) AND 
		(dbo.tblAPDN.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblAPDN.StatusID ELSE @sinStatusID END) AND
		(dbo.tblAPDN.VendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblAPDN.VendorID ELSE @intVendorID END) AND
		(dbo.tblAPDN.pdnDate <= CASE WHEN @datDate IS NULL THEN dbo.tblAPDN.pdnDate ELSE @datDate END))
	UNION ALL
	SELECT dbo.tblPO.VendorID,
		4 AS Type,
		dbo.tblPO.POID AS DocID,
		dbo.tblPO.POCD AS DocNo,
		'Prepaid' AS typDesc,
		dbo.tblAPPay.payDate AS DocDate,
		dbo.tblAPPay.payDate AS DocDueDate,
--By Antoni 16 Mar 2006
		'-' AS PONo,
--**************************** 
		- 1 AS TermPmtID,
		dbo.tblPO.CurrencyID,
--By Jemi 5 Oct 2005
		0 AS JournalTypeID,
--******************************
		dbo.tblAPPayStt.psdSttAmt * (-1) AS Amount,
		dbo.tblAPPay.COAID,
		dbo.tblAPPay.StatusID
	FROM dbo.tblPO INNER JOIN
		dbo.tblAPPayStt ON dbo.tblPO.POID = dbo.tblAPPayStt.psdRefID INNER JOIN
		dbo.tblAPPay ON dbo.tblAPPayStt.APPayID = dbo.tblAPPay.APPayID
	WHERE ((dbo.tblPO.StatusID <> 5) AND 
		(dbo.tblPO.Deleted =0) AND 
		(dbo.tblAPPayStt.psdType = 4) AND 
		(dbo.tblAPPay.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblAPPay.StatusID ELSE @sinStatusID END) AND
		(dbo.tblAPPay.StatusID <> 5) AND (dbo.tblAPPay.Deleted = 0) AND 
		(dbo.tblPO.VendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblPO.VendorID ELSE @intVendorID END) AND
		(dbo.tblAPPay.payDate <= CASE WHEN @datDate IS NULL THEN dbo.tblAPPay.payDate ELSE @datDate END))
)



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO



CREATE FUNCTION dbo.sfnARRptInvoiceSub
(
	@intVendorID	int,
	@sinStatusID	smallint, 
	@datDate 	datetime
)
RETURNS TABLE
AS
RETURN (
	SELECT dbo.tblInvoice.BillVendorID AS VendorID, 
		0 AS Type, 
		dbo.tblInvoice.InvoiceID AS DocID, 
		dbo.tblInvoice.InvoiceCD AS DocNo, 
		'Invoice' AS typDesc, 
		dbo.tblInvoice.ivoDocDate AS DocDate, 
		dbo.tblInvoice.ivoDueDate AS DocDueDate, 
--By Antoni 15 Mar 2006
--By Jemi 19 May 2006,  make lenght to 255 Max
		--ISNULL(dbo.sfnGetRefNoInvoice(dbo.tblInvoice.InvoiceID, 'PO', 0, ', '), '-') AS PONo,
		CONVERT(varchar(255), ISNULL(dbo.sfnGetRefNoInvoice(dbo.tblInvoice.InvoiceID, 'PO', 0, ', '), '-')) AS PONo,
--******************************
--******************************
		dbo.tblInvoice.TermPmtID, 
		dbo.tblInvoice.CurrencyID, 
--By Jemi 5 Oct 2005
		dbo.tblInvoice.JournalTypeID,
--******************************
		
		dbo.tblInvoice.ivoTotNetAmt AS Amount, 
		dbo.tblInvoice.COAID,
		StatusID 
	FROM dbo.tblInvoice 
	WHERE ((dbo.tblInvoice.Deleted = 0) AND 
		(dbo.tblInvoice.StatusID <> 5) AND 
		(dbo.tblInvoice.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblInvoice.StatusID ELSE @sinStatusID END) AND
		(dbo.tblInvoice.BillVendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblInvoice.BillVendorID ELSE @intVendorID END) AND
		(dbo.tblInvoice.ivoDocDate <= CASE WHEN @datDate IS NULL THEN dbo.tblInvoice.ivoDocDate ELSE @datDate END))
	UNION ALL
	SELECT dbo.tblARDN.VendorID, 
		1 AS Type, 
		dbo.tblARDN.ARDNID AS DocID, 
		dbo.tblARDN.ARDNCD AS DocNo, 
		'Debit Note' AS typDesc, 
		dbo.tblARDN.rdnDate AS DocDate, 
		dbo.tblARDN.rdnDate AS DocDueDate, 
--By Antoni 15 Mar 2006
		'-' AS PONo,
--******************************
		- 1 AS TermPmtID, 
		dbo.tblARDN.CurrencyID, 
--By Jemi 5 Oct 2005
		dbo.tblARDN.JournalTypeID,
--******************************
		dbo.tblARDN.rdnAmount AS Amount, 
		dbo.tblARDN.COAID,
		dbo.tblARDN.StatusID
	FROM dbo.tblARDN 
	WHERE ((dbo.tblARDN.StatusID <> 5) AND 
		(dbo.tblARDN.Deleted = 0) AND 
		(dbo.tblARDN.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblARDN.StatusID ELSE @sinStatusID END) AND
		(dbo.tblARDN.VendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblARDN.VendorID ELSE @intVendorID END) AND
		(dbo.tblARDN.rdnDate <= CASE WHEN @datDate IS NULL THEN dbo.tblARDN.rdnDate ELSE @datDate END))
	UNION ALL
	SELECT dbo.tblAR.VendorID, 
		2 AS Type, 
		dbo.tblAR.ARID AS DocID, 
		dbo.tblAR.arvDocNo AS DocNo, 
		'A/R' AS typDesc, 
		dbo.tblAR.arvDocDate AS DocDate, 
		dbo.tblAR.arvDueDate AS DocDueDate, 
--By Antoni 15 Mar 2006
		'-' AS PONo,
--******************************
		dbo.tblAR.TermPmtID, 
		dbo.tblAR.CurrencyID, 
--By Jemi 5 Oct 2005
		dbo.tblAR.JournalTypeID,
--******************************
		dbo.tblAR.arvAmount AS Amount, 
		dbo.tblAR.COAID,
		dbo.tblAR.StatusID
	FROM dbo.tblAR 
	WHERE ((dbo.tblAR.StatusID <> 5) AND 
		(dbo.tblAR.Deleted = 0) AND 
		(dbo.tblAR.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblAR.StatusID ELSE @sinStatusID END) AND
		(dbo.tblAR.VendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblAR.VendorID ELSE @intVendorID END) AND
		(dbo.tblAR.arvDocDate <= CASE WHEN @datDate IS NULL THEN dbo.tblAR.arvDocDate ELSE @datDate END))
	UNION ALL
	SELECT dbo.tblARCN.VendorID, 
		3 AS Type, 
		dbo.tblARCN.ARCNID AS DocID, 
		dbo.tblARCN.ARCNCD AS DocNo, 
		'Credit Note' AS typDesc, 
		dbo.tblARCN.rcnDate AS DocDate, 
		dbo.tblARCN.rcnDate AS DocDueDate, 
--By Antoni 15 Mar 2006
		'-' AS PONo,
--******************************
		- 1 AS TermPmtID, 
		dbo.tblARCN.CurrencyID, 
--By Jemi 5 Oct 2005
		dbo.tblARCN.JournalTypeID,
--******************************
		dbo.tblARCN.rcnAmount * (-1) AS Amount, 
		dbo.tblARCN.COAID,
		dbo.tblARCN.StatusID
	FROM dbo.tblARCN INNER JOIN
		dbo.tblVendor ON dbo.tblARCN.VendorID = dbo.tblVendor.VendorID
	WHERE ((dbo.tblARCN.StatusID <> 5) AND 
		(dbo.tblARCN.Deleted = 0) AND 
		(dbo.tblARCN.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblARCN.StatusID ELSE @sinStatusID END) AND
		(dbo.tblARCN.VendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblARCN.VendorID ELSE @intVendorID END) AND
		(dbo.tblARCN.rcnDate <= CASE WHEN @datDate IS NULL THEN dbo.tblARCN.rcnDate ELSE @datDate END))
	UNION ALL
	SELECT dbo.tblARRec.VendorID,
		4 AS Type,
		dbo.tblSO.SOID AS DocID,
		dbo.tblSO.SOCD AS DocNo,
		'Prepaid' AS typDesc,
		dbo.tblARRec.recDate AS DocDate,
		dbo.tblARRec.recDate AS DocDueDate,
--By Antoni 15 Mar 2006
		'-' AS PONo,
--******************************
		- 1 AS TermPmtID,
		dbo.tblSO.CurrencyID,
--By Jemi 5 Oct 2005
		0 AS JournalTypeID,
--******************************
		dbo.tblARRecStt.rsdSttAmt * (-1) AS Amount,
		dbo.tblARRec.COAID,
		dbo.tblARRec.StatusID
	FROM dbo.tblSO INNER JOIN 
		dbo.tblARRecStt ON dbo.tblSO.SOID = dbo.tblARRecStt.rsdRefID INNER JOIN
		dbo.tblARRec ON dbo.tblARRecStt.ARRecID = dbo.tblARRec.ARRecID
	WHERE ((dbo.tblSO.StatusID <> 5) AND 
		(dbo.tblSO.Deleted =0) AND 
		(dbo.tblARRecStt.rsdType = 4) AND 
		(dbo.tblARRec.StatusID = CASE WHEN @sinStatusID = -1 THEN dbo.tblARRec.StatusID ELSE @sinStatusID END) AND
		(dbo.tblARRec.StatusID <> 5) AND (dbo.tblARRec.Deleted = 0) AND 
		(dbo.tblARRec.VendorID = CASE WHEN @intVendorID = -1 THEN dbo.tblARRec.VendorID ELSE @intVendorID END) AND
		(dbo.tblARRec.recDate <= CASE WHEN @datDate IS NULL THEN dbo.tblARRec.recDate ELSE @datDate END))
)











GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.sfnAPRptInvoice
(
	@intVendorID 	int,
	@sinStatusID 	smallint,
	@datDate 	datetime = NULL
)
RETURNS TABLE
AS
RETURN 
(
	SELECT
		sfnAPRptInvoiceSub.VendorID, 
		dbo.tblVendor.vndViewName AS vndName, 
		sfnAPRptInvoiceSub.Type, 
		sfnAPRptInvoiceSub.typDesc, 
		sfnAPRptInvoiceSub.DocID, 
		sfnAPRptInvoiceSub.DocNo, 
		sfnAPRptInvoiceSub.DocDate, 
		sfnAPRptInvoiceSub.DocDueDate, 
--By Antoni 16 Mar 2006
		sfnAPRptInvoiceSub.PONo,
--***************************
		sfnAPRptInvoiceSub.TermPmtID, 
		dbo.tblTermPmt.tptName, 
		sfnAPRptInvoiceSub.CurrencyID, 
		dbo.tblCurrency.curName, 
		dbo.tblCurrency.curSymbol, 
		sfnAPRptInvoiceSub.Amount, 
		sfnAPRptPaymentSub.PaidDate, 
		CONVERT(decimal(24, 6), ISNULL(sfnAPRptPaymentSub.PaidAmt, 0)) AS PaidAmt, 
		sfnAPRptInvoiceSub.Amount - CONVERT(decimal(24, 6), ISNULL(sfnAPRptPaymentSub.PaidAmt, 0)) AS OSAmt, 
--By Jemi 14 Jul 2005
		dbo.tblVendor.vndCatID1,
		dbo.tblVendor.vndCatID2,
		dbo.tblVendor.vndCatID3,
--**********************************
--By Jemi 5 Oct 2005
		sfnAPRptInvoiceSub.JournalTypeID,
--***********************************
		sfnAPRptInvoiceSub.StatusID, 
		dbo.tblCOA.COAViewName,
		dbo.tblStatus.staName
	FROM dbo.sfnAPRptInvoiceSub(@intVendorID, @sinStatusID, @datDate) sfnAPRptInvoiceSub INNER JOIN
		dbo.tblVendor ON sfnAPRptInvoiceSub.VendorID = dbo.tblVendor.VendorID INNER JOIN
		dbo.tblStatus ON sfnAPRptInvoiceSub.StatusID = dbo.tblStatus.StatusID INNER JOIN
		dbo.tblCurrency ON sfnAPRptInvoiceSub.CurrencyID = dbo.tblCurrency.CurrencyID LEFT OUTER JOIN
		dbo.tblTermPmt ON sfnAPRptInvoiceSub.TermPmtID = dbo.tblTermPmt.TermPmtID LEFT OUTER JOIN
		dbo.sfnAPRptPaymentSub(@intVendorID, @datDate) sfnAPRptPaymentSub ON 
		sfnAPRptInvoiceSub.DocID = sfnAPRptPaymentSub.DocID AND sfnAPRptInvoiceSub.VendorID = sfnAPRptPaymentSub.VendorID AND sfnAPRptInvoiceSub.Type = sfnAPRptPaymentSub.Type  LEFT OUTER JOIN
		dbo.tblCOA ON sfnAPRptInvoiceSub.COAID = dbo.tblCOA.COAID 
)








GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO




CREATE FUNCTION dbo.sfnARRptInvoice
(
	@intVendorID	int,
	@sinStatusID	smallint,
	@datDate 	datetime
)
RETURNS TABLE
AS
RETURN 
(
	SELECT sfnARRptInvoiceSub.VendorID, 
		dbo.tblVendor.vndViewName AS vndName, 
		sfnARRptInvoiceSub.Type, 
		sfnARRptInvoiceSub.typDesc, 
		sfnARRptInvoiceSub.DocID, 
		sfnARRptInvoiceSub.DocNo, 
		sfnARRptInvoiceSub.DocDate, 
		sfnARRptInvoiceSub.DocDueDate, 
--By Antoni 15 Mar 2006
		sfnARRptInvoiceSub.PONo,
--******************************* 
		sfnARRptInvoiceSub.TermPmtID, 
		dbo.tblTermPmt.tptName, 
		sfnARRptInvoiceSub.CurrencyID, 
		dbo.tblCurrency.curName, 
		dbo.tblCurrency.curSymbol, 
		sfnARRptInvoiceSub.Amount, 
		sfnARRptReceiptSub.PaidDate, 
		CONVERT(decimal(24, 6), ISNULL(sfnARRptReceiptSub.PaidAmt, 0)) AS PaidAmt, 
		sfnARRptInvoiceSub.Amount - CONVERT(decimal(24, 6), ISNULL(sfnARRptReceiptSub.PaidAmt, 0)) AS OSAmt, 
--By Jemi 14 Jul 2005
		dbo.tblVendor.vndCatID1,
		dbo.tblVendor.vndCatID2,
		dbo.tblVendor.vndCatID3,
--**********************************
--By Jemi 5 Oct 2005
		sfnARRptInvoiceSub.JournalTypeID,
--***********************************
		sfnARRptInvoiceSub.StatusID, 
		dbo.tblCOA.COAViewName,
		dbo.tblStatus.staName
	FROM dbo.sfnARRptInvoiceSub(@intVendorID, @sinStatusID, @datDate) sfnARRptInvoiceSub INNER JOIN
		dbo.tblCOA ON sfnARRptInvoiceSub.COAID = dbo.tblCOA.COAID INNER JOIN
		dbo.tblVendor ON sfnARRptInvoiceSub.VendorID = dbo.tblVendor.VendorID INNER JOIN
		dbo.tblStatus ON sfnARRptInvoiceSub.StatusID = dbo.tblStatus.StatusID INNER JOIN
		dbo.tblCurrency ON sfnARRptInvoiceSub.CurrencyID = dbo.tblCurrency.CurrencyID LEFT OUTER JOIN
		dbo.tblTermPmt ON sfnARRptInvoiceSub.TermPmtID = dbo.tblTermPmt.TermPmtID LEFT OUTER JOIN
		dbo.sfnARRptReceiptSub(@intVendorID, @datDate) sfnARRptReceiptSub ON 
		sfnARRptInvoiceSub.DocID = sfnARRptReceiptSub.DocID AND sfnARRptInvoiceSub.VendorID = sfnARRptReceiptSub.VendorID AND sfnARRptInvoiceSub.Type = sfnARRptReceiptSub.Type 
)










GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

