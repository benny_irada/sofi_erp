"SELECT * FROM (
SELECT     dbo.tblPurchQuoPart.refid, dbo.tblPurchQuo.PurchQuoID, dbo.tblPurchQuo.PurchQuoCD, dbo.tblPurchQuo.pqtAttn, dbo.tblPurchQuo.pqtCc, 
                      dbo.tblTermPmt.TermPmtCD, dbo.tblTermDel.TermDelCD, dbo.tblEmployee.EmployeeCD, dbo.tblEmployee.empName, 
		      1 as A,'Unit Price' + Char(13) + dbo.tblCurrency.CurrencyCD + '(' + dbo.tblCurrency.curSymbol + ')' as Kolom,
		      dbo.tblPart.PartCD, dbo.tblPart.prtName, dbo.tblPart.prtViewName, 
                      dbo.tblPurchQuoPart.UnitID, dbo.tblUnit.UnitCD, dbo.tblUnit.uniName, left(convert(varchar,dbo.tblPurchQuoPart.pqpQuantity),len(convert(varchar,dbo.tblPurchQuoPart.pqpQuantity))-7) as pqpQuantity, dbo.tblPurchQuoPart.pqpPrice as Nilai, 
		      dbo.tblVendor.VendorCD, dbo.tblVendor.vndName, 
                      dbo.tblVendor.vndAddress, isnull(dbo.tblVendor.vndFax,'') as vndFax, dbo.tblVendor.vndPhone, isnull(dbo.tblVendor.vndContact,'-') as vndContact, dbo.tblPurchQuo.VendorID,
		      dbo.tblCountry.ctrName, dbo.tblCity.citName
FROM         dbo.tblPurchQuo INNER JOIN
                      dbo.tblPurchQuoPart ON dbo.tblPurchQuo.PurchQuoID = dbo.tblPurchQuoPart.PurchQuoID INNER JOIN
                      dbo.tblPart ON dbo.tblPurchQuoPart.PartID = dbo.tblPart.PartID INNER JOIN
                      dbo.tblUnit ON dbo.tblPurchQuoPart.UnitID = dbo.tblUnit.UnitID INNER JOIN
                      dbo.tblGrouping ON dbo.tblPurchQuoPart.GroupingID = dbo.tblGrouping.GroupingID INNER JOIN
                      dbo.tblVendor ON dbo.tblPurchQuo.VendorID = dbo.tblVendor.VendorID INNER JOIN
                      dbo.tblTermDel ON dbo.tblPurchQuo.TermDelID = dbo.tblTermDel.TermDelID INNER JOIN
                      dbo.tblTermPmt ON dbo.tblPurchQuo.TermPmtID = dbo.tblTermPmt.TermPmtID INNER JOIN
                      dbo.tblEmployee ON dbo.tblPurchQuo.EmployeeID = dbo.tblEmployee.EmployeeID INNER JOIN
                      dbo.tblCurrency ON dbo.tblPurchQuo.CurrencyID = dbo.tblCurrency.CurrencyID LEFT OUTER JOIN
					  dbo.tblCity ON dbo.tblVendor.CityID = dbo.tblCity.CityID LEFT OUTER JOIN 
					  dbo.tblCountry ON dbo.tblVendor.CountryID = dbo.tblCountry.CountryID
						WHERE dbo.tblPurchQuo.Deleted = 0
UNION ALL
SELECT     dbo.tblPurchQuoPart.refid, dbo.tblPurchQuo.PurchQuoID, dbo.tblPurchQuo.PurchQuoCD, dbo.tblPurchQuo.pqtAttn, dbo.tblPurchQuo.pqtCc, 
                      dbo.tblTermPmt.TermPmtCD, dbo.tblTermDel.TermDelCD, dbo.tblEmployee.EmployeeCD, dbo.tblEmployee.empName, 
		      2 as a,'Amount' + Char(13) + dbo.tblCurrency.CurrencyCD + '(' + dbo.tblCurrency.curSymbol + ')' as Kolom,
		      dbo.tblPart.PartCD, dbo.tblPart.prtName, dbo.tblPart.prtViewName, 
                      dbo.tblPurchQuoPart.UnitID, dbo.tblUnit.UnitCD, dbo.tblUnit.uniName, left(convert(varchar,dbo.tblPurchQuoPart.pqpQuantity),len(convert(varchar,dbo.tblPurchQuoPart.pqpQuantity))-7) as pqpQuantity, dbo.tblPurchQuoPart.pqpAmount as Nilai, 
		      dbo.tblVendor.VendorCD, dbo.tblVendor.vndName, 
                      dbo.tblVendor.vndAddress, isnull(dbo.tblVendor.vndFax,'') as vndFax, dbo.tblVendor.vndPhone, isnull(dbo.tblVendor.vndContact,'-') as vndContact, dbo.tblPurchQuo.VendorID,
		      dbo.tblCountry.ctrName, dbo.tblCity.citName
FROM         dbo.tblPurchQuo INNER JOIN
                      dbo.tblPurchQuoPart ON dbo.tblPurchQuo.PurchQuoID = dbo.tblPurchQuoPart.PurchQuoID INNER JOIN
                      dbo.tblPart ON dbo.tblPurchQuoPart.PartID = dbo.tblPart.PartID INNER JOIN
                      dbo.tblUnit ON dbo.tblPurchQuoPart.UnitID = dbo.tblUnit.UnitID INNER JOIN
                      dbo.tblGrouping ON dbo.tblPurchQuoPart.GroupingID = dbo.tblGrouping.GroupingID INNER JOIN
                      dbo.tblVendor ON dbo.tblPurchQuo.VendorID = dbo.tblVendor.VendorID INNER JOIN
                      dbo.tblTermDel ON dbo.tblPurchQuo.TermDelID = dbo.tblTermDel.TermDelID INNER JOIN
                      dbo.tblTermPmt ON dbo.tblPurchQuo.TermPmtID = dbo.tblTermPmt.TermPmtID INNER JOIN
                      dbo.tblEmployee ON dbo.tblPurchQuo.EmployeeID = dbo.tblEmployee.EmployeeID INNER JOIN
                      dbo.tblCurrency ON dbo.tblPurchQuo.CurrencyID = dbo.tblCurrency.CurrencyID LEFT OUTER JOIN
					  dbo.tblCity ON dbo.tblVendor.CityID = dbo.tblCity.CityID LEFT OUTER JOIN 
					  dbo.tblCountry ON dbo.tblVendor.CountryID = dbo.tblCountry.CountryID
			WHERE dbo.tblPurchQuo.Deleted = 0
UNION ALL
SELECT		dbo.tblPurchQuoPart.refid, dbo.tblPurchQuo.PurchQuoID, dbo.tblPurchQuo.PurchQuoCD, '', '', 
		'', '', '', '',
               	2 as a,'Amount' + Char(13) + dbo.tblCurrency.CurrencyCD + '(' + dbo.tblCurrency.curSymbol + ')' as Kolom,
               	'ZZZ', '             Sub Total Amount', '',
	       	'', '', '', '' , sum(pqpAmount) as Nilai,
               	dbo.tblVendor.VendorCD, dbo.tblVendor.vndName, dbo.tblVendor.vndAddress, isnull(dbo.tblVendor.vndFax,'') as vndFax, dbo.tblVendor.vndPhone, isnull(dbo.tblVendor.vndContact,'-') as vndContact, dbo.tblPurchQuo.VendorID,
               	dbo.tblCountry.ctrName, dbo.tblCity.citName 
FROM       dbo.tblPurchQuo INNER JOIN dbo.tblPurchQuoPart ON dbo.tblPurchQuo.PurchQuoID = dbo.tblPurchQuoPart.PurchQuoID INNER JOIN 
	       	dbo.tblPart ON dbo.tblPurchQuoPart.PartID = dbo.tblPart.PartID INNER JOIN 
	       	dbo.tblUnit ON dbo.tblPurchQuoPart.UnitID = dbo.tblUnit.UnitID INNER JOIN 
	       	dbo.tblGrouping ON dbo.tblPurchQuoPart.GroupingID = dbo.tblGrouping.GroupingID INNER JOIN 
	       	dbo.tblVendor ON dbo.tblPurchQuo.VendorID = dbo.tblVendor.VendorID INNER JOIN 
               	dbo.tblTermDel ON dbo.tblPurchQuo.TermDelID = dbo.tblTermDel.TermDelID INNER JOIN 
		dbo.tblTermPmt ON dbo.tblPurchQuo.TermPmtID = dbo.tblTermPmt.TermPmtID INNER JOIN 
		dbo.tblEmployee ON dbo.tblPurchQuo.EmployeeID = dbo.tblEmployee.EmployeeID INNER JOIN 
		dbo.tblCurrency ON dbo.tblPurchQuo.CurrencyID = dbo.tblCurrency.CurrencyID LEFT OUTER JOIN
               	dbo.tblCity ON dbo.tblVendor.CityID = dbo.tblCity.CityID LEFT OUTER JOIN       
             	dbo.tblCountry ON dbo.tblVendor.CountryID = dbo.tblCountry.CountryID
		WHERE dbo.tblPurchQuo.Deleted = 0
		group by dbo.tblPurchQuoPart.refid, dbo.tblPurchQuo.PurchQuoID, dbo.tblPurchQuo.PurchQuoCD, dbo.tblCurrency.CurrencyCD, dbo.tblCurrency.curSymbol, dbo.tblVendor.VendorCD, dbo.tblVendor.vndName, dbo.tblVendor.vndAddress, isnull(dbo.tblVendor.vndFax,''), dbo.tblVendor.vndPhone, dbo.tblVendor.vndContact, dbo.tblPurchQuo.VendorID,
               	dbo.tblCountry.ctrName, dbo.tblCity.citName
) tblPurchQuoPart"