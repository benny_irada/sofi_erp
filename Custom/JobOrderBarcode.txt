"SELECT     TOP 100 PERCENT 1 AS ID, dbo.tblProdOrder.ProdOrderCD, Right(Convert(varchar(255), dbo.tblProdOrderDet.SeqID),12) AS Barcode, 
dbo.tblProdOrder.prodSTartDate AS invDate, dbo.tblPart.PartCD, dbo.tblPart.prtName, 
ISNULL(dbo.tblMachine.macName,'') AS macName,dbo.tblMachine.Macline as Macline, dbo.tblWork.worName, '' AS empName, 'GENERAL' AS lotName, dbo.tblProdOrder.Station,
dbo.sfnGetDecToString(dbo.tblProdOrder.prodQuantity) + ' ' + dbo.tblUnit.UnitCD AS Quantity, 
dbo.sfnGetRefRouting(Convert(varchar(255),dbo.tblProdOrderDet.SeqID), 'NextProcess') AS NextProcess,
Mat.PartCD AS MatPartCD, Mat.prtName AS MatPrtName,
dbo.sfnGetDecToString(Convert(decimal,dbo.tblProdDetail.prdQuantity*dbo.tblProdOrder.prodQuantity)) + ' ' + UnitMat.UnitCD AS prodQuantity 
FROM dbo.tblProdOrder
INNER JOIN (Select ProdOrderID, MIN(podSeq) AS podSeq From dbo.tblProdOrderDet Group By ProdOrderID) 
OrderDet ON dbo.tblProdOrder.ProdOrderID=OrderDet.ProdOrderID 
INNER JOIN dbo.tblProdOrderDet ON OrderDet.ProdOrderID=dbo.tblProdOrderDet.ProdOrderID AND OrderDet.podSeq=dbo.tblProdOrderDet.podSeq
INNER JOIN dbo.tblProdRouting ON dbo.tblProdOrderDet.RefSeqID=dbo.tblProdRouting.SeqID 
INNER JOIN dbo.tblPart ON dbo.tblProdRouting.PartID = dbo.tblPart.PartID 
INNER JOIN dbo.tblUnit ON dbo.tblPart.UnitID=dbo.tblUnit.UnitID 
INNER JOIN dbo.tblProdDetail ON dbo.tblProdRouting.SeqID=tblProdDetail.RefSeqID
INNER JOIN dbo.tblPart Mat ON dbo.tblProdDetail.PartID=Mat.PartID
INNER JOIN dbo.tblUnit unitMat ON dbo.tblProdDetail.UnitID=unitMat.UnitID
INNER JOIN dbo.tblWork ON dbo.tblProdRouting.WorkID=dbo.tblWork.WorkID 
LEFT OUTER JOIN dbo.tblMachine ON dbo.tblprodorder.MachineID=dbo.tblMachine.MachineID"