"SELECT     
TOP 100 PERCENT 1 AS ID, prrSeq, 
Right((Select Top 1 Convert(varchar(255),SeqID) FROM tblProdOrderDet NextRoute Where ProdOrderID=tblProdOrderDet.ProdOrderID AND podSeq>dbo.tblProdOrderDet.podSeq Order by podSeq),12) AS Barcode,
dbo.tblInventory.InventoryCD, 
dbo.tblInventory.invDate, dbo.tblInventory.invNote, dbo.tblPart.PartCD, 
dbo.tblPart.prtName, dbo.tblPart.prtNote, dbo.tblUnit.UnitCD,
ISNULL(dbo.tblMachine.macName,'') AS macName, 
dbo.tblWork.worName, dbo.tblEmployee.empName, dbo.tblProdOrder.Station,
dbo.tblLot.lotName, LotPart.LotCD AS LotPart,
convert(decimal(6,2),dbo.tblInventory.invDocument2) AS Quantity, 
(convert(decimal(6,2),dbo.tblInventory.invDocument2)*0.9) AS QtyRequired, 
dbo.sfnGetRefRouting(Convert(varchar(255),dbo.tblProdOrderDet.SeqID), 'NextProcess') AS NextProcess,
Mat.PartCD AS MatPartCD, Mat.prtName AS MatPrtName, unitMat.UnitCD AS UnitMat,
dbo.tblInvPart.inpQuantity AS prodQuantity,
prdQuantity AS mdpQuantity,
tblprodorder.prodpriority,
(Select Count(*) from tblInventory FIFO Where Deleted=0 and invDocument1=Convert(varchar(255),dbo.tblProdOrderDet.SeqID) AND InventoryID<tblInventory.InventoryID)+1 AS FIFO,
(select partcd from tblpart where tblproduction.partid= tblpart.partid)as FGPartCD,
(select prtname from tblpart where tblproduction.partid= tblpart.partid)as FGPartName
FROM dbo.tblInventory 
INNER JOIN dbo.tblProdOrderDet ON dbo.tblInventory.invDocument1=Convert(varchar(255),dbo.tblProdOrderDet.SeqID)
INNER JOIN dbo.tblProdOrder ON dbo.tblProdOrder.ProdOrderID=dbo.tblProdOrderDet.ProdOrderID
INNER JOIN dbo.tblPart ON dbo.tblProdOrder.PartID = dbo.tblPart.PartID
INNER JOIN dbo.tblUnit ON dbo.tblPart.UnitID=dbo.tblUnit.UnitID
INNER JOIN dbo.tblProdRouting ON dbo.tblProdOrderDet.RefSeqID=dbo.tblProdRouting.SeqID
INNER JOIN dbo.tblInvPart ON dbo.tblInventory.InventoryID=tblInvPart.InventoryID
INNER JOIN dbo.tblPart Mat ON dbo.tblInvPart.PartID=Mat.PartID
INNER JOIN dbo.tblUnit unitMat ON dbo.tblInvPart.UnitID=unitMat.UnitID
INNER JOIN dbo.tblEmployee ON dbo.tblInventory.EmployeeID=dbo.tblEmployee.EmployeeID
INNER JOIN dbo.tblWork ON dbo.tblProdRouting.WorkID=dbo.tblWork.WorkID
INNER JOIN dbo.tblLot ON dbo.tblInventory.invDocument3=dbo.tblLot.LotID
INNER JOIN dbo.tblLot LotPart ON dbo.tblInvPart.LotID=LotPart.LotID
LEFT OUTER JOIN dbo.tblMachine ON dbo.tblProdRouting.MachineID=dbo.tblMachine.MachineID
INNER JOIN dbo.tblproduction on dbo.tblprodrouting.productionid = dbo.tblproduction.productionid
INNER JOIN dbo.tblProdDetail on dbo.tblproduction.ProductionID = dbo.tblProdDetail.ProductionID and dbo.tblProdDetail.PartID = Mat.PartID
"